const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const projectSchema= new Schema({
    userID:{
        type:String,
    },
    name:{
        type:String,
    },
    address:{
        type:String,
    },
    occupation:{
        type:String,
    },
    email:{
        type:String,
    }
});
const data = mongoose.model('data',projectSchema);
module.exports = data