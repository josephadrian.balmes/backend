const express = require('express');
const app = express();
const cors = require('cors')
const mongoose = require('mongoose');

//import db model
const Data = require('./model/data')
const SchedData = require('./model/sched')

app.use(cors())
app.use(express.urlencoded({extended: true})); 
app.use(express.json());  

let port = process.env.PORT || 3000;

mongoose.connect('mongodb+srv://admin:admin123@cluster0.vli80.mongodb.net/test', 
{
  useNewUrlParser: true,
  useUnifiedTopology: true 
}).then(()=>{
  console.log("Connected to db")
}).catch(e=>{
  console.log("No Connection"+ e)
})

app.get('/data',async(req,res)=>{
    
    let BACKEND = await Data.find({});
    res.send(BACKEND)
})

app.get('/scheds',async(req,res)=>{
    
    let BACKEND = await SchedData.find({});
    res.send(BACKEND)
})

//For adding new data

app.post('/data',async(req,res)=>{
    try{
        const addNewPost = new Data(req.body);
        // console.log(req.body)
        const insertPost = await addNewPost.save();
        res.send(insertPost)
      }catch(e){
        res.send(e)
      }
})

app.post('/sched',async(req,res)=>{
    try{
        const addNewPost = new SchedData(req.body);
        // console.log(req.body)
        const insertPost = await addNewPost.save();
        res.send(insertPost)
      }catch(e){
        res.send(e)
      }
})

app.listen(port,(req,res)=>{
    console.log(`Listening to port ${port}`) 
})